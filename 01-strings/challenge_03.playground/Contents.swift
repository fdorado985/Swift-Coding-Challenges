//: Playground - noun: a place where people can play

import UIKit

// Challengue 03 : Do two strings contain the characters?

/* Description ============
 Write a function that accepts two String parameters, and returns true if they contain the same characters in any order taking into account letter case.
 ========================= */

/* Rules ===================
 • The strings “abca” and “abca” should return true.
 • The strings “abc” and “cba” should return true.
 • The strings “ a1 b2 ” and “b1 a2” should return true.
 • The strings “abc” and “abca” should return false.
 • The strings “abc” and “Abc” should return false.
 • The strings “abc” and “cbAa” should return false.
 ========================== */


/* Challenge ============== */

func challenge3(input1: String, input2: String) -> Bool {
    let trimmedInput1 = input1.trimmingCharacters(in: .whitespacesAndNewlines)
    let trimmedInput2 = input2.trimmingCharacters(in: .whitespacesAndNewlines)
    
    let arrayInput1 = Array(trimmedInput1).sorted()
    let arrayInput2 = Array(trimmedInput2).sorted()
    
    return arrayInput1 == arrayInput2
}


// MARK: Tests to evaluate function

assert(challenge3(input1: "abca", input2: "abca") == true)
assert(challenge3(input1: "abc", input2: "cba") == true)
assert(challenge3(input1: " a1 b2 ", input2: "b1 a2") == true)
assert(challenge3(input1: "abc", input2: "abca") == false)
assert(challenge3(input1: "abc", input2: "Abc") == false)
assert(challenge3(input1: "abc", input2: "cbAa") == false)
