//: Playground - noun: a place where people can play

import UIKit

// Challengue 02 : Is a string a palindrome?

/* Description ============
 Write a function that accepts a String as its only parameter, and returns true if the string reads the same when reversed, ignoring case.
 ========================= */

/* Rules ===================
 • The string “rotator” should return true.
 • The string “Rats live on no evil star” should return true.
 • The string “Never odd or even” should return false; even though the letters are the same in reverse the spaces are in different places.
 • The string “Hello, world” should return false because it reads “dlrow ,olleH” backwards.
 ========================== */


/* Challenge ============== */

func challenge2(input: String) -> Bool {
    let reversedInput = String(input.lowercased().reversed())
    return reversedInput == input.lowercased()
}


// MARK: Tests to evaluate function
assert(challenge2(input: "rotator") == true)
assert(challenge2(input: "Rats live on no evil star") == true)
assert(challenge2(input: "Never odd or even") == false)
assert(challenge2(input: "Hello, world") == false)
