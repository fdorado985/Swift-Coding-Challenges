//: Playground - noun: a place where people can play

import UIKit

// Challengue 04 : Does one string contain another?

/* Description ============
 Write your own version of the contains() method on String that ignores letter case, and without using the existing contains() method.
 ========================= */

/* Rules ===================
 • The code "Hello, world".fuzzyContains("Hello") should return true.
 • The code "Hello, world".fuzzyContains("WORLD") should return true.
 • The code "Hello, world".fuzzyContains("Goodbye") should return false.
 ========================== */


/* Challenge ============== */

extension String {
    
    func fuzzyContains(input: String) -> Bool {
        return self.lowercased().range(of: input.lowercased()) != nil
    }
}


// MARK: Tests to evaluate function

assert("Hello, world".fuzzyContains(input: "Hello") == true)
assert("Hello, world".fuzzyContains(input: "WORLD") == true)
assert("Hello, world".fuzzyContains(input: "Goodbye") == false)

