//: Playground - noun: a place where people can play

import UIKit

// Challengue 05 : Count the characters

/* Description ============
 Write a function that accepts a string, and returns how many times a specific character appears, taking case into account.
 ========================= */

/* Rules ===================
 • The letter “a” appears twice in “The rain in Spain”.
 • The letter “i” appears four times in “Mississippi”.
 • The letter “i” appears three times in “Hacking with Swift”.
========================== */


/* Challenge ============== */

func challenge5(input: String, search: Character) -> Int {
    let countedSet = NSCountedSet()
    
    for character in input {
        countedSet.add(character)
    }
    
    return countedSet.count(for: search)
}

func challenge5a(input: String, search: String) -> Int {
    let array = input.map { String($0) }
    let counted = NSCountedSet(array: array)
    return counted.count(for: search)
}


// MARK: Tests to evaluate function

assert(challenge5(input: "The rain in Spain", search: "a") == 2)
assert(challenge5(input: "Mississippi", search: "i") == 4)
assert(challenge5(input: "Hacking with Swift", search: "i") == 3)

