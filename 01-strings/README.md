# Strings
The list of challenges in here are covering the part of `strings`

## Challenges
### Easy
1. Are the letters unique?
2. Is a string a palindrome?
3. Do two strings contain the same characters?
4. Does one string contain another?
5. Count the characters
6. Remove duplicate letters from a string
7. Condense whitespace
