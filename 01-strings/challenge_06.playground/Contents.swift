//: Playground - noun: a place where people can play

import UIKit

// Challengue 06 : Remove duplicate letter from a string
// Difficulty: Easy

/* Description ============
 Write a function that accepts a string as its input, and returns the same string just with duplicate letters removed.
 
 Tip: If you can solve this challenge without a for-in loop, you can consider it “tricky” rather than “easy”.
 ========================= */

/* Rules ===================
 • The string “wombat” should print “wombat”.
 • The string “hello” should print “helo”.
 • The string “Mississippi” should print “Misp”.
 ========================== */


/* Challenge ============== */

func challenge6(input: String) -> String {
    var newString = ""
    var usedLetters = [Character]()
    for character in input {
        if !usedLetters.contains(character) {
            usedLetters.append(character)
            newString.append(character)
        }
    }
    return newString
}


// MARK: Tests to evaluate function

assert(challenge6(input: "wombat") == "wombat")
assert(challenge6(input: "hello") == "helo")
assert(challenge6(input: "Missisipi") == "Misp")

