<p align="center">
  <img src="assets/swift_coding_challenge_logo.png" title="Logo">
</p>

---

[![platform_ios](assets/platform.png)](https://www.apple.com/ios/ios-11-preview/)
[![ide_xcode9](assets/ide.png)](https://developer.apple.com/xcode)
[![twitter](assets/twitter.png)](https://twitter.com/fdorado985)
[![instagram](assets/instagram.png)](https://www.instagram.com/juan_fdorado)

Here you will be able to see a couple of challenges to work with.

## Table Of Content
1. [Strings](01-strings/)
2. [Numbers]()
2. [Files]()
2. [Collections]()
2. [Algorithms]()
